package plugins

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

const (
	pluginDirEnv = "SKELLY_PLUGINS_DIR"
)

var (
	executable   string
	pluginPrefix string
)

func init() {
	e, _ := os.Executable()
	executable = filepath.Base(e)
	pluginPrefix = executable + "-"
}

type PluginFinder interface {
	FindPlugin(name string) (string, error)
	FindAllPlugins() []string
}

type LocalPluginFinder struct {
	pluginDirectory string
	pluginPrefix    string
}

func NewLocalPluginFinder() PluginFinder {
	return LocalPluginFinder{
		pluginDirectory: getPluginDirectory(),
		pluginPrefix:    pluginPrefix,
	}
}

func (l LocalPluginFinder) FindPlugin(name string) (string, error) {
	// check our plugin directory
	cmd := pluginPrefix + name
	dir := getPluginDirectory()
	path := filepath.Join(dir, cmd)
	info, err := os.Stat(path)
	fmt.Printf("%+v\n", info)
	if err == nil && isExecAny(info.Mode()) {
		return path, nil
	}

	// look in the environment PATH
	path, err = exec.LookPath(cmd)
	if err == nil {
		return path, nil
	}

	return "", fmt.Errorf("cannot find plugin in PATH or %s: %s", dir, cmd)
}

func (d LocalPluginFinder) FindAllPlugins() []string {
	result := []string{}
	paths := filepath.SplitList(os.Getenv("PATH"))
	if dir := getPluginDirectory(); dir != "" {
		paths = append(paths, dir)
	}

	for _, path := range paths {
		fmt.Printf("Processing path: %s\n", path)
		files, err := os.ReadDir(path)
		if err != nil {
			continue
		}

		for _, file := range files {
			if strings.HasPrefix(file.Name(), pluginPrefix) {
				base := strings.TrimLeft(file.Name(), pluginPrefix)
				if contains(result, base) {
					continue
				}
				fpath := filepath.Join(path, file.Name())
				info, err := os.Stat(fpath)
				if err != nil {
					continue
				}
				if isExecAny(info.Mode()) {
					result = append(result, base)
				}
			}
		}
	}

	return result
}

func getPluginDirectory() string {
	dir := os.Getenv(pluginDirEnv)
	if dir != "" {
		return dir
	}
	if xdgHome := os.Getenv("XDG_DATA_HOME"); xdgHome != "" {
		return filepath.Join(xdgHome, executable, "plugins")
	}
	homedir, err := os.UserHomeDir()
	if err != nil {
		return ""
	}
	return filepath.Join(homedir, ".config", executable, "plugins")
}

// executable by user, group, other
func isExecAny(mode os.FileMode) bool {
	return mode&0111 != 0
}

func contains(s []string, v string) bool {
	for _, a := range s {
		if a == v {
			return true
		}
	}
	return false
}
