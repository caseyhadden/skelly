package plugins

import (
	"fmt"
	"os"
	"testing"

	"gotest.tools/v3/assert"
	"gotest.tools/v3/fs"
)

func TestGetPluginDirectory(t *testing.T) {
	executable = "findertest"
	homedir, _ := os.UserHomeDir()
	tests := map[string]struct {
		xdgDataHome      string
		skellyPluginsDir string
		want             string
	}{
		"default": {want: homedir + "/.config/findertest/plugins"},
		"xdg":     {xdgDataHome: "/xdgtest", want: "/xdgtest/findertest/plugins"},
		"environ": {skellyPluginsDir: "/skellyplugins", want: "/skellyplugins"},
	}
	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			t.Setenv("XDG_DATA_HOME", tc.xdgDataHome)
			t.Setenv("SKELLY_PLUGINS_DIR", tc.skellyPluginsDir)
			dir := getPluginDirectory()
			assert.Equal(t, tc.want, dir)
		})
	}
}

func TestIsExecAny(t *testing.T) {
	tests := map[string]struct {
		mode os.FileMode
		want bool
	}{
		"user-x":         {mode: 0o100, want: true},
		"group-x":        {mode: 0o010, want: true},
		"other-x":        {mode: 0o001, want: true},
		"user-rx":        {mode: 0o300, want: true},
		"group-rx":       {mode: 0o030, want: true},
		"other-rx":       {mode: 0o003, want: true},
		"user-wx":        {mode: 0o500, want: true},
		"group-wx":       {mode: 0o050, want: true},
		"other-wx":       {mode: 0o005, want: true},
		"user-all":       {mode: 0o700, want: true},
		"group-all":      {mode: 0o070, want: true},
		"other-all":      {mode: 0o007, want: true},
		"all":            {mode: 0o777, want: true},
		"not-executable": {mode: 0o666, want: false},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			assert.Equal(t, tc.want, isExecAny(tc.mode))
		})
	}
}

func TestFindPlugin(t *testing.T) {
	pluginPrefix = "findertest-"

	tests := map[string]struct {
		mode      os.FileMode
		filename  string
		expectErr bool
	}{
		"standard":               {mode: 0o700, filename: "findertest-plugin", expectErr: false},
		"ignores-non-executable": {mode: 0o600, filename: "findertest-plugin", expectErr: true},
		"invalid-name":           {mode: 0o700, filename: "invalid-plugin", expectErr: true},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			tmpfs := fs.NewDir(t, fmt.Sprintf("TestFindPlugin-%s", name))
			defer tmpfs.Remove()
			t.Setenv(pluginDirEnv, tmpfs.Path())
			err := os.WriteFile(tmpfs.Join(tc.filename), []byte("doesnt-matter"), tc.mode)
			assert.NilError(t, err)
			finder := NewLocalPluginFinder()
			path, err := finder.FindPlugin("plugin")
			if tc.expectErr {
				assert.Assert(t, err != nil)
			} else {
				assert.NilError(t, err)
				assert.Equal(t, path, tmpfs.Join(tc.filename))
			}
		})
	}
}

func TestFindPluginInPath(t *testing.T) {
	pluginPrefix = "findertest-"

	tmpfs := fs.NewDir(t, "TestFindPluginInPath")
	defer tmpfs.Remove()
	t.Setenv("PATH", tmpfs.Path())
	err := os.WriteFile(tmpfs.Join("findertest-plugin"), []byte("doesnt-matter"), 0o700)
	assert.NilError(t, err)
	finder := NewLocalPluginFinder()
	path, err := finder.FindPlugin("plugin")
	assert.NilError(t, err)
	assert.Equal(t, path, tmpfs.Join("findertest-plugin"))
}

func TestFindAllPlugins(t *testing.T) {
	pluginPrefix = "findertest-"

	tests := map[string]struct {
		numFolders       int
		pluginsPerFolder int
		wantLen          int
	}{
		"no-plugins":  {numFolders: 1, wantLen: 0},
		"one-plugins": {numFolders: 1, pluginsPerFolder: 1, wantLen: 1},
		"two-plugins": {numFolders: 1, pluginsPerFolder: 2, wantLen: 2},
		"two-folders": {numFolders: 2, pluginsPerFolder: 1, wantLen: 2},
	}

	for name, tc := range tests {
		path := ""
		for i := 0; i < tc.numFolders; i++ {
			tmpfs := fs.NewDir(t, fmt.Sprintf("TestFindAllPlugins-%s-%d", name, i))
			defer tmpfs.Remove()
			path = path + ":" + tmpfs.Path()
			for j := 0; j < tc.pluginsPerFolder; j++ {
				filename := fmt.Sprintf("%splugin-%d-%d", pluginPrefix, i, j)
				err := os.WriteFile(tmpfs.Join(filename), []byte("doesnt-matter"), 0o700)
				assert.NilError(t, err)
			}
		}
		t.Setenv("PATH", path)

		finder := NewLocalPluginFinder()
		result := finder.FindAllPlugins()
		assert.Equal(t, tc.wantLen, len(result))
	}
}
