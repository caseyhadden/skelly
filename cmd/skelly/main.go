package main

import (
	"fmt"
	"os"
	"syscall"

	"github.com/caseyhadden/skelly/pkg/plugins"
)

func main() {
	fmt.Println("Skelly CLI...")

	pluginFinder := plugins.NewLocalPluginFinder()
	if len(os.Args) == 1 {
		fmt.Printf("%+v\n", pluginFinder.FindAllPlugins())
	} else {
		p, err := pluginFinder.FindPlugin(os.Args[1])
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to find plugin: %v", err)
			os.Exit(1)
		}

		// replace current process with plugin
		if err := syscall.Exec(p, append([]string{p}, os.Args[2:]...), os.Environ()); err != nil {
			fmt.Fprintf(os.Stderr, "Command finished with error: %v", err)
			os.Exit(127)
		}
	}
}
