# ![skelly from hades](skelly.png) skelly

skelly is a command line interface (cli) and library implementing a common cli
pattern - a root command which finds plugins on the system PATH.

**NOTE: skelly is in the ideation and implementation phase. We may rewrite history.**

## why skelly?

![skelly's room](skelly-room.png)

1. Fun - I enjoy roguelite games. The repetitive gameplay of roguelites (live,
   learn, die, repeat) has some similarity with programming. You're constantly
   learning in both and improving (hopefully!) with each attempt. One of the
   most popular roguelites has been
   [Hades](https://en.wikipedia.org/wiki/Hades_(video_game)), and
   [Skelly](https://hades.fandom.com/wiki/Skelly) is an NPC that sits in the
   room where you choose your tools for a run, is helpful, and celebrates your
   successes in the game.
2. Skeleton - the project tends more towards a skeleton style. It's a place you
   hang your important logic off of vs. being the most important thing itself.
3. Poetic license - In the English language, the letter 'c' makes a couple of
   different sounds, /s/ and /k/. If you take the acronym, cli, and use some
   poetic license to imagine the 'c' making both sounds, and insert the missing
   vowel in order to pronounce it as a word, you can get to 'skeli.' And that
   is close to 'skelly'.

## features & use cases

1. take the skelly binary and go
2. do the connecting back and forth for stdout/stderr/rc between the root
   skelly cli and a plugin
3. rename the skelly binary to build your own plugins
4. use the skelly libraries to create your own root cli
5. provide a binary to manage plugins in the local filesystem
6. provide libraries to manage plugins in more complex scenarios,
   such as installing from a remote source.

